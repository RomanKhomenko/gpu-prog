#include <helper/error.cuh>
#include <helper/vector.cuh>

#include <iostream>
#include <iomanip>
#include <vector>

__global__
void multiply(helper::vector<double> v1, helper::vector<double> v2) {
    int index = blockDim.x * blockIdx.x + threadIdx.x;
    int offset = gridDim.x * blockDim.x;

    for (int i = index; i < v1.size(); i += offset) {
        v2[i] *= v1[i];
    }
}

int main() {
    uint64_t n = 0;

    std::ios::sync_with_stdio(false);
    std::cin >> n;

    std::vector<double> h_v1 = std::vector<double>(n);
    std::vector<double> h_v2 = std::vector<double>(n);

    for (uint64_t i = 0; i < h_v1.size(); i++) {
        std::cin >> h_v1[i];
    }

    for (uint64_t i = 0; i < h_v2.size(); i++) {
        std::cin >> h_v2[i];
    }

    helper::vector<double> d_v1(h_v1.size(), h_v1.data());
    helper::vector<double> d_v2(h_v2.size(), h_v2.data());

    multiply<<<256, 256>>>(d_v1, d_v2);
    cudaDeviceSynchronize();
    CUDA_CHECK_LAST_ERROR;

    d_v2.to_host(h_v2.data());

    d_v1.free();
    d_v2.free();

    std::cout << std::scientific << std::setprecision(10);
    for (uint64_t i = 0; i < h_v2.size(); i++) {
        std::cout << h_v2[i] << " ";
    }

    std::cout << std::endl;
}
