// clang-format off
#include <GL/glew.h>
#include <GL/freeglut.h>
// clang-format on

#include <algorithm>
#include <chrono>
#include <vector>
#include <random>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#include <boost/algorithm/minmax_element.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

const int MAX_POINT_COUNT = 64;

struct double2 {
    double2() : x{}, y{} {}
    double2(double xx, double yy) : x{xx}, y{yy} {}

    double x;
    double y;
};

using point = double2;

struct double3 {
    double x;
    double y;
    double z;
};

struct int2 {
    int x;
    int y;
};

struct uchar4 {
    uint8_t x;
    uint8_t y;
    uint8_t z;
    uint8_t w;
};

struct particle {
    double2 v;
    double2 x;
    double2 coulomb_force;
    double3 best;
};

inline double2 make_double2(double x, double y) {
    return double2{.x = x, .y = y};
}

inline double3 make_double3(double x, double y, double z) {
    return double3{.x = x, .y = y, .z = z};
}

inline int2 make_int2(int x, int y) { return int2{.x = x, .y = y}; }

inline uchar4 make_uchar4(uint8_t x, uint8_t y, uint8_t z, uint8_t w) {
    return uchar4{.x = x, .y = y, .z = z, .w = w};
}

struct compare_particles {
    bool operator()(particle left, particle right) {
        return left.best.z < right.best.z;
    }
};

struct particles_sum_op {
    particle operator()(particle left, particle right) {
        left.x.x += right.x.x;
        left.x.y += right.x.y;

        return left;
    }
};

static const double DT = 0.01;
static const int SEED = 0xdeadbeaf;

static int g_width{};
static int g_height{};

static int g_dev_width;
static int g_dev_height;
static int g_min_count;
static double g_min_values[MAX_POINT_COUNT];
static double2 g_min_pos[MAX_POINT_COUNT];
static double g_min;
static double g_max;
static double2 g_scale;
static double2 g_dev_scale;
static double2 g_shift;
static double2 g_dev_shift;
static double g_color_delta{};
static double g_dev_color_time;

static double g_dev_a1;
static double g_dev_a2;
static double g_dev_w;

static double g_dev_epsilon;
static double g_dev_k;

static particle g_dev_best;
static std::vector<particle>* g_particles;

inline double square(double x) { return x * x; }

double shekel(double2 coords) {
    double res = 0;
    for (int i = 0; i < g_min_count; i++) {
        double tmp = g_min_values[i] + square(coords.x - g_min_pos[i].x) +
                     square(coords.y - g_min_pos[i].y);
        res += 1.0 / tmp;
    }

    return res;
}

double2 get_coords_by_index(int2 index) {
    return make_double2(
        -g_dev_scale.x * (2.0 * index.x / (g_dev_width - 1) - 1.0) +
            g_dev_shift.x,
        -g_dev_scale.y * (2.0 * index.y / (g_dev_height - 1) - 1.0) +
            g_dev_shift.y);
}

int2 get_index_by_coords(double2 coords) {
    return make_int2((-(coords.x - g_dev_shift.x) / g_dev_scale.x + 1) *
                         (g_dev_width - 1) * 0.5,
                     (-(coords.y - g_dev_shift.y) / g_dev_scale.y + 1) *
                         (g_dev_height - 1) * 0.5);
}

double shekel(int i, int j) {
    return shekel(get_coords_by_index(make_int2(i, j)));
}

uchar4 get_color(double f) {
    const double s = 100 * cos(g_dev_color_time);
    const int v = 100;
    const double vmin = ((100.0 - s) * v / 100.0) * 255.0 / 100;

    int hi = static_cast<int>(f * 250 / 60) % 6;
    double a = (v - vmin) * (static_cast<int>(f * 250) % 60) / 60.0;
    double vinc = (vmin + a) * 255.0 / 100;
    double vdec = (v - a) * 255.0 / 100;

    switch (hi) {
        case 0:
            return make_uchar4(v, vinc, vmin, 0);
        case 1:
            return make_uchar4(vdec, v, vmin, 0);
        case 2:
            return make_uchar4(vmin, v, vinc, 0);
        case 3:
            return make_uchar4(vmin, vdec, v, 0);
        case 4:
            return make_uchar4(vinc, vmin, v, 0);
        case 5:
            return make_uchar4(v, vmin, vdec, 0);
        default:
            return make_uchar4(1, 1, 1, 1);
    }
}

void kernel() {
    glBegin(GL_POINTS);
    for (int i = 0; i < g_dev_width; i++) {
        for (int j = 0; j < g_dev_height; j++) {
            double f = (shekel(i, j) - g_min) / (g_max - g_min);
            auto c = get_color(f);
            glColor4ub(c.x, c.y, c.z, c.w);
            glVertex2i(i, j);
        }
    }
    glEnd();
}

void init_particles_kernel(particle* particles, int n) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);

    for (int i = 0; i < n; i++) {
        int2 index = make_int2((g_dev_width - 1) * dis(gen),
                               (g_dev_height - 1) * dis(gen));
        particles[i].x = get_coords_by_index(index);
        particles[i].v.x = 400 * dis(gen) - 200;
        particles[i].v.y = 400 * dis(gen) - 200;
        particles[i].best.x = 0;
        particles[i].best.y = 0;
        particles[i].best.z = -1;
    }
}

void draw_particles_kernel(particle* particles, int n) {
    const uchar4 point_color = make_uchar4(255, 255, 255, 0);

    glPointSize(4);
    glBegin(GL_POINTS);
    for (int i = 0; i < n; i++) {
        int2 index = get_index_by_coords(particles[i].x);

        if (index.x < 2 || index.x > g_dev_width - 2 || index.y < 2 ||
            index.y > g_dev_height - 2) {
            continue;
        }

        glColor4ub(point_color.x, point_color.y, point_color.z, point_color.w);
        glVertex2i(index.x, index.y);
    }
    glEnd();
}

void coulomb_force_kernel(particle* particles, int n) {
    for (int i = 0; i < n; i++) {
        double fx = 0;
        double fy = 0;
        particle* pi = &particles[i];
        for (int j = 0; j < n; j++) {
            particle* pj = &particles[j];
            double r = hypot(pi->x.x - pj->x.x, pi->x.y - pj->x.y);

            fx += (pi->x.x - pj->x.x) / (r * r * r * r + g_dev_epsilon);
            fy += (pi->x.y - pj->x.y) / (r * r * r * r + g_dev_epsilon);
        }

        pi->coulomb_force = make_double2(fx, fy);
    }
}

void move_particles_kernel(particle* particles, int n) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);

    for (int i = 0; i < n; i++) {
        particle* p = &particles[i];
        double r1 = dis(gen);
        double r2 = dis(gen);

        p->v.x = g_dev_w * p->v.x + (g_dev_a1 * r1 * (p->best.x - p->x.x) +
                                     g_dev_a2 * r2 * (g_dev_best.x.x - p->x.x) -
                                     g_dev_k * p->coulomb_force.x) *
                                        DT;
        p->v.y = g_dev_w * p->v.y + (g_dev_a1 * r1 * (p->best.y - p->x.y) +
                                     g_dev_a2 * r2 * (g_dev_best.x.y - p->x.y) -
                                     g_dev_k * p->coulomb_force.y) *
                                        DT;
        p->x.x += p->v.x * DT;
        p->x.y += p->v.y * DT;

        double f = shekel(p->x);
        if (f > p->best.z) {
            p->best.x = p->x.x;
            p->best.y = p->x.y;
            p->best.z = f;
        }
    }
}

void init_draw(int width, int height, const std::vector<double>& c,
               const std::vector<point>& pos, double min, double max,
               double color_delta, double a1, double a2, double w, int n,
               double eps, double k) {
    static_assert(sizeof(double2) == sizeof(point),
                  "Point size != double2 size!");

    g_width = width;
    g_height = height;
    g_color_delta = color_delta;

    int size = c.size();

    g_min_count = size;
    g_min = min;
    g_max = max;

    std::copy(std::begin(c), std::end(c), g_min_values);
    std::copy(std::begin(pos), std::end(pos), g_min_pos);

    g_dev_height = g_height;
    g_dev_width = g_width;

    g_dev_scale = g_scale = make_double2(15.407, 15.407);
    g_dev_shift = g_shift = make_double2(0, 0);

    g_dev_a1 = a1;
    g_dev_a2 = a2;

    g_dev_w = w;
    g_dev_epsilon = eps;
    g_dev_k = k;

    g_particles = new std::vector<particle>(n);

    init_particles_kernel(g_particles->data(), g_particles->size());

    particle* p = std::max_element(g_particles->data(), g_particles->data() + n,
                                   compare_particles());

    g_dev_best = *p;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, static_cast<GLdouble>(width), 0,
               static_cast<GLdouble>(height));
}

void update() {
    auto start = std::chrono::high_resolution_clock::now();
    kernel();
    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "draw_heatmap: "
              << std::chrono::duration_cast<std::chrono::nanoseconds>(end -
                                                                      start)
                  .count()
              << std::endl;

    particle* p = std::max_element(g_particles->data(),
                                   g_particles->data() + g_particles->size(),
                                   compare_particles());
    g_dev_best = *p;

    start = std::chrono::high_resolution_clock::now();
    draw_particles_kernel(g_particles->data(), g_particles->size());
    end = std::chrono::high_resolution_clock::now();

    std::cout << "draw_particle: "
              << std::chrono::duration_cast<std::chrono::nanoseconds>(end -
                                                                      start)
                     .count()
              << std::endl;

    start = std::chrono::high_resolution_clock::now();
    coulomb_force_kernel(g_particles->data(), g_particles->size());
    end = std::chrono::high_resolution_clock::now();


    std::cout << "coulomb_force: "
        <<std::chrono::duration_cast<std::chrono::nanoseconds>(end -
                                                                      start)
                     .count()
              << std::endl;

    start = std::chrono::high_resolution_clock::now();
    move_particles_kernel(g_particles->data(), g_particles->size());
    end = std::chrono::high_resolution_clock::now();

    std::cout << "move_particles: "
    <<std::chrono::duration_cast<std::chrono::nanoseconds>(end -
                                                                      start)
                     .count()
              << std::endl;

    particle mass_center =
        std::accumulate(g_particles->begin(), g_particles->end(), particle(),
                        particles_sum_op());
    mass_center.x.x /= g_particles->size();
    mass_center.x.y /= g_particles->size();

    g_dev_shift = mass_center.x;

    glutPostRedisplay();

    g_dev_color_time += g_color_delta;
}

void display() {
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    glDrawPixels(g_width, g_height, GL_RGBA, GL_UNSIGNED_BYTE, 0);

    update();

    glutSwapBuffers();
    glFlush();
}

void keys(uint8_t key, int x, int y) {
    const int QUIT = 'q';
    const int SCALE_UP = 'k';
    const int SCALE_DOWN = 'j';
    const double SCALE_DELTA = 1.2;

    if (key == QUIT) {
        delete g_particles;
        std::exit(EXIT_SUCCESS);
    } else if (key == SCALE_UP) {
        g_dev_scale = g_scale =
            make_double2(g_scale.x * SCALE_DELTA, g_scale.y * SCALE_DELTA);
    } else if (key == SCALE_DOWN) {
        g_dev_scale = g_scale =
            make_double2(g_scale.x / SCALE_DELTA, g_scale.y / SCALE_DELTA);
    }
}

namespace po = boost::program_options;
namespace pt = boost::property_tree;

int main(int argc, char** argv) {
    int width{};
    int height{};
    double color_delta{};
    double w{};
    double a1{};
    double a2{};
    int n{};
    double eps{};
    double k{};
    std::string config{};
    std::vector<point> mins_pos;
    std::vector<double> mins;

    try {
        const char* desc_str =
            "CUDA Curse Project.\n"
            "Use \'q\' to quit, \'j\',\'k\' - scale down,scale up.\n"
            "See default Shekel function config file \"mins.json\" "
            "for help with config.\n"
            "Options";

        po::options_description desc(desc_str);
        // clang-format off
        desc.add_options()
        ("help", "print this message and exit")
        ("width,w", po::value<int>()->default_value(1024), "window width")
        ("height,h", po::value<int>()->default_value(648), "window height")
        ("config,c", po::value<std::string>()->default_value("mins.json"),
            "set minimums config file")
        ("freq,f", po::value<double>()->default_value(0),
            "color frequency delta")
        ("a1,a", po::value<double>()->default_value(0.5), "a1 constant")
        ("a2,b", po::value<double>()->default_value(0.5), "a2 constant")
        ("mass,m", po::value<double>()->default_value(0.5), "particle mass")
        ("count,n", po::value<int>()->default_value(1000), "particles count")
        ("eps,e", po::value<double>()->default_value(1e-5),
            "Coulomb force epsilon")
        ("const,k", po::value<double>()->default_value(0.1),
            "Coulomb force k");
        // clang-format on

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << std::endl;
            std::exit(EXIT_SUCCESS);
        }

        width = vm["width"].as<int>();
        height = vm["height"].as<int>();
        config = vm["config"].as<std::string>();
        color_delta = vm["freq"].as<double>();
        a1 = vm["a1"].as<double>();
        a2 = vm["a2"].as<double>();
        w = vm["mass"].as<double>();
        n = vm["count"].as<int>();
        eps = vm["eps"].as<double>();
        k = vm["const"].as<double>();
    } catch (const po::error& e) {
        std::cout << "Bad command line arguments! Try \"--help\" for help."
                  << std::endl;
        std::exit(EXIT_FAILURE);
    }

    std::fstream fs;
    fs.open(config, std::fstream::in);

    if (!fs.is_open()) {
        std::cout << "Cannot open config file \"" << config << "\"!"
                  << std::endl;
        std::exit(EXIT_FAILURE);
    }

    try {
        pt::ptree pt;
        pt::read_json(fs, pt);

        for (const auto& ms : pt.get_child("mins")) {
            for (const auto& m : ms.second) {
                auto value = m.second.get<double>("value");

                std::vector<double> pos;
                for (const auto& item : m.second.get_child("pos")) {
                    pos.push_back(item.second.get_value<double>());
                }

                if (pos.size() != 2) {
                    std::cout << "Bad minimum position (value = " << value
                              << ")!" << std::endl;
                    std::exit(EXIT_FAILURE);
                }

                mins.push_back(value);
                mins_pos.emplace_back(pos[0], pos[1]);
            }
        }
    } catch (pt::ptree_error const& e) {
        std::cout << "Config parser error: " << e.what() << std::endl;
        fs.close();
        std::exit(EXIT_FAILURE);
    }
    fs.close();

    if (mins.size() != mins_pos.size()) {
        std::cout << "Bad config file: count of min != count of min positions!"
                  << std::endl;
        std::exit(EXIT_FAILURE);
    }

    auto minmax = boost::minmax_element(std::begin(mins), std::end(mins));

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(width, height);
    glutCreateWindow("CUDA Course project");

    glutDisplayFunc(display);
    glutKeyboardFunc(keys);

    glewInit();

    init_draw(width, height, mins, mins_pos, *minmax.first, *minmax.second,
              color_delta, a1, a2, w, n, eps, k);

    glutMainLoop();

    return 0;
}