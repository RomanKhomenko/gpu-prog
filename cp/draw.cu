#include "draw.hpp"

// clang-format off
#include <GL/glew.h>
#include <GL/freeglut.h>
// clang-format on

#include <cuda_gl_interop.h>
#include <curand.h>
#include <curand_kernel.h>

#include <thrust/extrema.h>
#include <thrust/device_vector.h>
#include <thrust/reduce.h>

#include "error.cuh"

namespace th = thrust;

struct particle {
    double2 v;
    double2 x;
    double2 coulomb_force;
    double3 best;
};

struct compare_particles {
    __host__ __device__ bool operator()(particle left, particle right) {
        return left.best.z < right.best.z;
    }
};

struct particles_sum_op {
    __host__ __device__ particle operator()(particle left, particle right) {
        left.x.x += right.x.x;
        left.x.y += right.x.y;

        return left;
    }
};

static const double DT = 0.01;
static const int SEED = 0xdeadbeaf;

static int g_width{};
static int g_height{};
static GLuint g_vbo{};
static cudaGraphicsResource* g_res{};

static __constant__ int g_dev_width;
static __constant__ int g_dev_height;
static __constant__ int g_min_count;
static __constant__ double g_min_values[MAX_POINT_COUNT];
static __constant__ double2 g_min_pos[MAX_POINT_COUNT];
static __constant__ double g_min;
static __constant__ double g_max;
static double2 g_scale;
static __device__ double2 g_dev_scale;
static double2 g_shift;
static __device__ double2 g_dev_shift;
static double g_color_delta{};
static __device__ double g_dev_color_time;

static __constant__ double g_dev_a1;
static __constant__ double g_dev_a2;
static __constant__ double g_dev_w;

static __constant__ double g_dev_epsilon;
static __constant__ double g_dev_k;

static __device__ particle g_dev_best;
static th::device_vector<double>* g_func_values;
static th::device_vector<particle>* g_particles;
static th::device_vector<curandState>* g_curand_states;

__device__ __inline__ double square(double x) { return x * x; }

__device__ double shekel(double2 coords) {
    double res = 0;
    for (int i = 0; i < g_min_count; i++) {
        double tmp = g_min_values[i] + square(coords.x - g_min_pos[i].x) +
                     square(coords.y - g_min_pos[i].y);
        res += 1.0 / tmp;
    }

    return res;
}

__device__ double2 get_coords_by_index(int2 index) {
    return make_double2(
        -g_dev_scale.x * (2.0 * index.x / (g_dev_width - 1) - 1.0) +
            g_dev_shift.x,
        -g_dev_scale.y * (2.0 * index.y / (g_dev_height - 1) - 1.0) +
            g_dev_shift.y);
}

__device__ int2 get_index_by_coords(double2 coords) {
    return make_int2((-(coords.x - g_dev_shift.x) / g_dev_scale.x + 1) *
                         (g_dev_width - 1) * 0.5,
                     (-(coords.y - g_dev_shift.y) / g_dev_scale.y + 1) *
                         (g_dev_height - 1) * 0.5);
}

__device__ double shekel(int i, int j) {
    return shekel(get_coords_by_index(make_int2(i, j)));
}

__device__ uchar4 get_color(double f) {
//    const double s = 100 * cos(g_dev_color_time);
//    const int v = 100;
//    const double vmin = ((100.0 - s) * v / 100.0) * 255.0 / 100;
//
//    int hi = static_cast<int>(f * 250 / 60) % 6;
//    double a = (v - vmin) * (static_cast<int>(f * 250) % 60) / 60.0;
//    double vinc = (vmin + a) * 255.0 / 100;
//    double vdec = (v - a) * 255.0 / 100;
//
//    switch (hi) {
//        case 0:
//            return make_uchar4(v, vinc, vmin, 0);
//        case 1:
//            return make_uchar4(vdec, v, vmin, 0);
//        case 2:
//            return make_uchar4(vmin, v, vinc, 0);
//        case 3:
//            return make_uchar4(vmin, vdec, v, 0);
//        case 4:
//            return make_uchar4(vinc, vmin, v, 0);
//        case 5:
//            return make_uchar4(v, vmin, vdec, 0);
//        default:
//            return make_uchar4(1, 1, 1, 1);
//    }
    double k = 1.0 / 6.0;
    if (f < k) return make_uchar4((int)(f * 255 / k), 0, 0, 0);
    if (f < 2 * k) return make_uchar4(255, (int)((f - k) * 255 / k), 0, 0);
    if (f < 3 * k)
        return make_uchar4(255, 255, (int)((f - 2 * k) * 255 / k), 0);
    if (f < 4 * k)
        return make_uchar4(255 - (int)((f - 3 * k) * 255 / k), 255, 255, 0);
    if (f < 5 * k)
        return make_uchar4(0, 255 - (int)((f - 4 * k) * 255 / k), 255, 0);
    if (f < 6 * k)
        return make_uchar4(0, 0, 255 - (int)((f - 5 * k) * 255 / k), 0);
    return make_uchar4(0, 0, 0, 0);
}

__global__ void kernel(uchar4* data) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int idy = blockIdx.y * blockDim.y + threadIdx.y;
    int offsetx = blockDim.x * gridDim.x;
    int offsety = blockDim.y * gridDim.y;
    for (int i = idx; i < g_dev_width; i += offsetx) {
        for (int j = idy; j < g_dev_height; j += offsety) {
            double f = (shekel(i, j) - g_min) / (g_max - g_min);
            data[j * g_dev_width + i] = get_color(f);
        }
    }
}

__global__ void init_particles_kernel(particle* particles, int n, int seed,
                                      curandState* state) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int offsetx = blockDim.x * gridDim.x;

    for (int i = idx; i < n; i += offsetx) {
        curand_init(seed, idx, 0, &state[i]);

        int2 index = make_int2((g_dev_width - 1) * curand_uniform(&state[i]),
                               (g_dev_height - 1) * curand_uniform(&state[i]));
        particles[i].x = get_coords_by_index(index);
        particles[i].v.x = 400 * curand_uniform(&state[i]) - 200;
        particles[i].v.y = 400 * curand_uniform(&state[i]) - 200;
        particles[i].best.x = 0;
        particles[i].best.y = 0;
        particles[i].best.z = -1;
    }
}

__global__ void draw_particles_kernel(particle* particles, int n,
                                      uchar4* data) {
    const uchar4 point_color = make_uchar4(255, 255, 255, 0);

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int offsetx = blockDim.x * gridDim.x;

    for (int i = idx; i < n; i += offsetx) {
        int2 index = get_index_by_coords(particles[i].x);

        if (index.x < 2 || index.x > g_dev_width - 2 || index.y < 2 ||
            index.y > g_dev_height - 2) {
            continue;
        }

        data[index.y * g_dev_width + index.x] = point_color;
        data[(index.y + 1) * g_dev_width + (index.x + 1)] = point_color;
        data[(index.y + 1) * g_dev_width + (index.x)] = point_color;
        data[(index.y) * g_dev_width + (index.x + 1)] = point_color;
    }
}

__global__ void coulomb_force_kernel(particle* particles, int n) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int offsetx = blockDim.x * gridDim.x;

    for (int i = idx; i < n; i += offsetx) {
        double fx = 0;
        double fy = 0;
        particle* pi = &particles[i];
        for (int j = 0; j < n; j++) {
            particle* pj = &particles[j];
            double r = hypot(pi->x.x - pj->x.x, pi->x.y - pj->x.y);

            fx += (pi->x.x - pj->x.x) / (r * r * r * r + g_dev_epsilon);
            fy += (pi->x.y - pj->x.y) / (r * r * r * r + g_dev_epsilon);
        }

        pi->coulomb_force = make_double2(fx, fy);
    }
}

__global__ void move_particles_kernel(particle* particles, int n, int seed,
                                      curandState* states) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int offsetx = blockDim.x * gridDim.x;

    for (int i = idx; i < n; i += offsetx) {
        particle* p = &particles[i];
        double r1 = curand_uniform(&states[i]);
        double r2 = curand_uniform(&states[i]);

        p->v.x = g_dev_w * p->v.x + (g_dev_a1 * r1 * (p->best.x - p->x.x) +
                                     g_dev_a2 * r2 * (g_dev_best.x.x - p->x.x) -
                                     g_dev_k * p->coulomb_force.x) *
                                        DT;
        p->v.y = g_dev_w * p->v.y + (g_dev_a1 * r1 * (p->best.y - p->x.y) +
                                     g_dev_a2 * r2 * (g_dev_best.x.y - p->x.y) -
                                     g_dev_k * p->coulomb_force.y) *
                                        DT;
        p->x.x += p->v.x * DT;
        p->x.y += p->v.y * DT;

        double f = shekel(p->x);
        if (f > p->best.z) {
            p->best.x = p->x.x;
            p->best.y = p->x.y;
            p->best.z = f;
        }
    }
}

__global__ void update_minmax_kernel(double* data) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int idy = blockIdx.y * blockDim.y + threadIdx.y;
    int offsetx = blockDim.x * gridDim.x;
    int offsety = blockDim.y * gridDim.y;
    for (int i = idx; i < g_dev_width; i += offsetx) {
        for (int j = idy; j < g_dev_height; j += offsety) {
            data[j * g_dev_width + i] = shekel(i, j);
        }
    }
}

void init_draw(int width, int height, const std::vector<double>& c,
               const std::vector<point>& pos, double min, double max,
               double color_delta, double a1, double a2, double w, int n,
               double eps, double k) {
    cudaError_t err{};

    static_assert(sizeof(double2) == sizeof(point),
                  "Point size != double2 size!");

    g_width = width;
    g_height = height;
    g_color_delta = color_delta;

    int size = c.size();

    err = cudaMemcpyToSymbol(g_min_count, &size, sizeof(size));
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_min, &min, sizeof(min));
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_max, &max, sizeof(max));
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_min_values, c.data(), c.size() * sizeof(double));
    CUDA_CHECK_ERROR(err);

    err =
        cudaMemcpyToSymbol(g_min_pos, pos.data(), pos.size() * sizeof(double2));
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_dev_height, &g_height, sizeof(g_height));
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_dev_width, &g_width, sizeof(g_width));
    CUDA_CHECK_ERROR(err);

    g_scale = make_double2(15.407, 15.407);
    g_shift = make_double2(0, 0);

    err = cudaMemcpyToSymbol(g_dev_scale, &g_scale, sizeof(g_scale));
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_dev_shift, &g_shift, sizeof(g_shift));
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_dev_a1, &a1, sizeof(a1));
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_dev_a2, &a2, sizeof(a2));
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_dev_w, &w, sizeof(w));
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_dev_epsilon, &eps, sizeof(eps));
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_dev_k, &k, sizeof(k));
    CUDA_CHECK_ERROR(err);

    g_func_values = new th::device_vector<double>(g_width * g_height);

    g_particles = new th::device_vector<particle>(n);

    g_curand_states = new th::device_vector<curandState>(n);
    init_particles_kernel<<<256, 256>>>(
        th::raw_pointer_cast(g_particles->data()), g_particles->size(), SEED,
        th::raw_pointer_cast(g_curand_states->data()));
    CUDA_CHECK_LAST_ERROR;

    th::device_ptr<particle> p = th::max_element(
        g_particles->data(), g_particles->data() + n, compare_particles());

    err = cudaMemcpyToSymbol(g_dev_best, th::raw_pointer_cast(p),
                             sizeof(struct particle), 0,
                             cudaMemcpyDeviceToDevice);
    CUDA_CHECK_ERROR(err);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, static_cast<GLdouble>(width), 0,
               static_cast<GLdouble>(height));

    glGenBuffers(1, &g_vbo);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, g_vbo);
    glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, width * height * sizeof(uchar4),
                 nullptr, GL_DYNAMIC_DRAW);

    err = cudaGraphicsGLRegisterBuffer(&g_res, g_vbo,
                                       cudaGraphicsMapFlagsWriteDiscard);
    CUDA_CHECK_ERROR(err);
}

void update() {
    uchar4* dev_data;
    size_t size;

    cudaError_t err = cudaGraphicsMapResources(1, &g_res, 0);
    CUDA_CHECK_ERROR(err);
    err = cudaGraphicsResourceGetMappedPointer((void**)&dev_data, &size, g_res);
    CUDA_CHECK_ERROR(err);

    th::device_ptr<particle> p = th::max_element(
        g_particles->data(), g_particles->data() + g_particles->size(),
        compare_particles());

    err = cudaMemcpyToSymbol(g_dev_best, th::raw_pointer_cast(p),
                             sizeof(struct particle), 0,
                             cudaMemcpyDeviceToDevice);
    CUDA_CHECK_ERROR(err);

    particle best;
    err = cudaMemcpyFromSymbol(&best, g_dev_best, sizeof(particle));
    CUDA_CHECK_ERROR(err);

    coulomb_force_kernel<<<256, 256>>>(
        th::raw_pointer_cast(g_particles->data()), g_particles->size());
    CUDA_CHECK_LAST_ERROR;

    move_particles_kernel<<<256, 256>>>(
        th::raw_pointer_cast(g_particles->data()), g_particles->size(), SEED,
        th::raw_pointer_cast(g_curand_states->data()));
    CUDA_CHECK_LAST_ERROR;

    particle mass_center = th::reduce(g_particles->begin(), g_particles->end(),
                                      particle(), particles_sum_op());
    mass_center.x.x /= g_particles->size();
    mass_center.x.y /= g_particles->size();

    std::cout << mass_center.x.x << ", " << mass_center.x.y << std::endl;

    err =
        cudaMemcpyToSymbol(g_dev_shift, &mass_center.x, sizeof(mass_center.x));
    CUDA_CHECK_ERROR(err);

    update_minmax_kernel<<<dim3(32, 32), dim3(16, 16)>>>(
        th::raw_pointer_cast(g_func_values->data()));
    CUDA_CHECK_LAST_ERROR;

    auto pair = th::minmax_element(
        g_func_values->data(), g_func_values->data() + g_func_values->size());

    err = cudaMemcpyToSymbol(g_min, th::raw_pointer_cast(pair.first),
                             sizeof(g_min), 0, cudaMemcpyDeviceToDevice);
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_max, th::raw_pointer_cast(pair.second),
                             sizeof(g_max), 0, cudaMemcpyDeviceToDevice);
    CUDA_CHECK_ERROR(err);

    kernel<<<dim3(32, 32), dim3(16, 16)>>>(dev_data);
    CUDA_CHECK_LAST_ERROR;

    draw_particles_kernel<<<256, 256>>>(
        th::raw_pointer_cast(g_particles->data()), g_particles->size(),
            dev_data);
    CUDA_CHECK_LAST_ERROR;

    err = cudaGraphicsUnmapResources(1, &g_res, 0);
    CUDA_CHECK_ERROR(err);

    glutPostRedisplay();

    double t;
    err = cudaMemcpyFromSymbol(&t, g_dev_color_time, sizeof(t));
    CUDA_CHECK_ERROR(err);

    t += g_color_delta;

    err = cudaMemcpyToSymbol(g_dev_color_time, &t, sizeof(t));
    CUDA_CHECK_ERROR(err);
}

void display() {
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    glDrawPixels(g_width, g_height, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glutSwapBuffers();
}

#define MAYBE_UNUSED __attribute__((unused))

void keys(uint8_t key, MAYBE_UNUSED int x, MAYBE_UNUSED int y) {
    const int QUIT = 'q';
    const int SCALE_UP = 'k';
    const int SCALE_DOWN = 'j';
    const double SCALE_DELTA = 1.2;

    if (key == QUIT) {
        auto err = cudaGraphicsUnregisterResource(g_res);
        CUDA_CHECK_ERROR(err);

        glBindBuffer(1, g_vbo);
        glDeleteBuffers(1, &g_vbo);

        delete g_particles;
        delete g_curand_states;

        std::exit(EXIT_SUCCESS);
    } else if (key == SCALE_UP) {
        g_scale =
            make_double2(g_scale.x * SCALE_DELTA, g_scale.y * SCALE_DELTA);
        auto err =
            cudaMemcpyToSymbol(g_dev_scale, &g_scale, sizeof(g_dev_scale));
        CUDA_CHECK_ERROR(err);
    } else if (key == SCALE_DOWN) {
        g_scale =
            make_double2(g_scale.x / SCALE_DELTA, g_scale.y / SCALE_DELTA);

        auto err =
            cudaMemcpyToSymbol(g_dev_scale, &g_scale, sizeof(g_dev_scale));
        CUDA_CHECK_ERROR(err);
    }
}
