#!/usr/bin/env bash

optirun ./cmake-build-debug/cp -w 1920 -h 1080 -k -5e-3 -n 5000 -m 0.99 -e 1e-5 --a1 0.4 --a2 0.6
