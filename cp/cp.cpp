#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#ifndef CHECKER
#include <boost/algorithm/minmax_element.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

namespace po = boost::program_options;
namespace pt = boost::property_tree;
#endif

extern "C" {
// clang-format off
#include <GL/glew.h>
#include <GL/freeglut.h>
// clang-format on
}

#include "draw.hpp"

int main(int argc, char** argv) {
    int width{};
    int height{};
    double color_delta{};
    double w{};
    double a1{};
    double a2{};
    int n{};
    double eps{};
    double k{};
    std::string config{};
    std::vector<point> mins_pos;
    std::vector<double> mins;

#ifndef CHECKER
    try {
        const char* desc_str =
            "CUDA Curse Project.\n"
            "Use \'q\' to quit, \'j\',\'k\' - scale down,scale up.\n"
            "See default Shekel function config file \"mins.json\" "
            "for help with config.\n"
            "Options";

        po::options_description desc(desc_str);
        // clang-format off
        desc.add_options()
        ("help", "print this message and exit")
        ("width,w", po::value<int>()->default_value(1024), "window width")
        ("height,h", po::value<int>()->default_value(648), "window height")
        ("config,c", po::value<std::string>()->default_value("mins.json"),
            "set minimums config file")
        ("freq,f", po::value<double>()->default_value(0),
            "color frequency delta")
        ("a1,a", po::value<double>()->default_value(0.5), "a1 constant")
        ("a2,b", po::value<double>()->default_value(0.5), "a2 constant")
        ("mass,m", po::value<double>()->default_value(0.5), "particle mass")
        ("count,n", po::value<int>()->default_value(1000), "particles count")
        ("eps,e", po::value<double>()->default_value(1e-5),
            "Coulomb force epsilon")
        ("const,k", po::value<double>()->default_value(0.1),
            "Coulomb force k");
        // clang-format on

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << std::endl;
            std::exit(EXIT_SUCCESS);
        }

        width = vm["width"].as<int>();
        height = vm["height"].as<int>();
        config = vm["config"].as<std::string>();
        color_delta = vm["freq"].as<double>();
        a1 = vm["a1"].as<double>();
        a2 = vm["a2"].as<double>();
        w = vm["mass"].as<double>();
        n = vm["count"].as<int>();
        eps = vm["eps"].as<double>();
        k = vm["const"].as<double>();
    } catch (const po::error& e) {
        std::cout << "Bad command line arguments! Try \"--help\" for help."
                  << std::endl;
        std::exit(EXIT_FAILURE);
    }

    std::fstream fs;
    fs.open(config, std::fstream::in);

    if (!fs.is_open()) {
        std::cout << "Cannot open config file \"" << config << "\"!"
                  << std::endl;
        std::exit(EXIT_FAILURE);
    }

    try {
        pt::ptree pt;
        pt::read_json(fs, pt);

        for (const auto& ms : pt.get_child("mins")) {
            for (const auto& m : ms.second) {
                auto value = m.second.get<double>("value");

                std::vector<double> pos;
                for (const auto& item : m.second.get_child("pos")) {
                    pos.push_back(item.second.get_value<double>());
                }

                if (pos.size() != 2) {
                    std::cout << "Bad minimum position (value = " << value
                              << ")!" << std::endl;
                    std::exit(EXIT_FAILURE);
                }

                mins.push_back(value);
                mins_pos.emplace_back(pos[0], pos[1]);
            }
        }
    } catch (pt::ptree_error const& e) {
        std::cout << "Config parser error: " << e.what() << std::endl;
        fs.close();
        std::exit(EXIT_FAILURE);
    }
    fs.close();

    if (mins.size() != mins_pos.size()) {
        std::cout << "Bad config file: count of min != count of min positions!"
                  << std::endl;
        std::exit(EXIT_FAILURE);
    }
#else
    k = -5e-3;
    n = 5000;
    w = 0.99;
    eps = 1e-5;
    a1 = 0.4;
    a2 = 0.6;
    width = 1920;
    height = 1080;

    mins = {0.3, 0.7, 0.1, 0.9, 0.5};

    mins_pos.emplace_back(0, 0);
    mins_pos.emplace_back(5, 5);
    mins_pos.emplace_back(15, 0);
    mins_pos.emplace_back(0, 10);
    mins_pos.emplace_back(10, 10);
#endif

    auto minmax = std::minmax_element(std::begin(mins), std::end(mins));

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(width, height);
    glutCreateWindow("CUDA Course project");

    glutIdleFunc(update);
    glutDisplayFunc(display);
    glutKeyboardFunc(keys);

    glewInit();

    init_draw(width, height, mins, mins_pos, *minmax.first, *minmax.second,
              color_delta, a1, a2, w, n, eps, k);

    glutMainLoop();

    return 0;
}