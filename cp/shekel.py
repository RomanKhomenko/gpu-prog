import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

def shekel(c, a, m, x, y):
    res = 0.0
    for i in range(0, m):
        tmp = c[i] + (x - a[i][0]) ** 2 + (y - a[i][1]) ** 2
        res += 1.0 / tmp
    return res

c = np.array([0.45, 0.1, 0.3, 0.2, 0.7])
a = np.array([[0, 0], [5, 5], [15, 0], [0, 10], [10, 10]])

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
x = y = np.arange(-20, 20, 0.05)
X, Y = np.meshgrid(x, y)
zs = np.array(shekel(c, a, 5, np.ravel(X), np.ravel(Y)))
Z = zs.reshape(X.shape)

ax.plot_surface(X, Y, Z)

plt.show()
