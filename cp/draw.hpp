#ifndef CP_DRAW_HPP_
#define CP_DRAW_HPP_

#include <cinttypes>
#include <vector>

struct point {
    point(double xx, double yy) : x{xx}, y{yy} {}

    double x;
    double y;
};

enum { MAX_POINT_COUNT = 64 };

void init_draw(int width, int height, const std::vector<double>& c,
               const std::vector<point>& pos, double min, double max,
               double color_delta, double a1, double a2, double w, int n,
               double eps, double k);
void update();
void display();
void keys(uint8_t key, int x, int y);

#endif  // CP_DRAW_HPP_
