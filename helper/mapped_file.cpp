#include <helper/mapped_file.hpp>

#include <cerrno>
#include <cstring>
#include <tuple>

static inline std::string generate_error_message(const std::string& msg) {
    auto err_msg = msg + ": " + std::string(strerror(errno));
    return err_msg;
}

helper::mapped_file::mapped_file()
    : data_{nullptr}, size_{0}, fd_{0} {}


uint64_t helper::mapped_file::parse_fd_flags(uint64_t flags) {
    uint64_t fd_flags{};
    if (flags & open_flags::ro) {
        mmap_flags_ = PROT_READ;
        fd_flags = O_RDONLY;
    }
    else if (flags & open_flags::wo) {
        mmap_flags_ = PROT_WRITE;
        fd_flags = O_WRONLY;
    }
    else if (flags & open_flags::rw) {
        mmap_flags_ = PROT_READ | PROT_WRITE;
        fd_flags = O_RDWR;
    }
    else {
        throw std::runtime_error("bad file flags");
    }

    return fd_flags;
}


void helper::mapped_file::open(const std::string& path,
                               uint64_t flags,
                               uint64_t size) {
    auto fd_flags = parse_fd_flags(flags);

    fd_ = ::open(path.c_str(), fd_flags, 0600);
    if (fd_ < 0) {
        throw std::runtime_error(generate_error_message("Cannot open file"));
    }

    size_ = size;

    if (flags & open_flags::c) {
        ftruncate(fd_, size);
    }
    
    map(size);
}

void helper::mapped_file::open(int fd,
                               uint64_t flags,
                               uint64_t size) {
    parse_fd_flags(flags);

    fd_ = fd;
    size_ = size;

    map(size);
}

void helper::mapped_file::close() {
    unmap(); 

    ::close(fd_);
    data_ = nullptr;
    size_ = 0;
    mmap_flags_ = 0;
}

uint64_t helper::mapped_file::size() const {
    return size_;
}

void helper::mapped_file::remap(uint64_t size) {
    unmap();
    map(size);
    
    size_ = size;
}

uint8_t* helper::mapped_file::begin() {
    return data_;
}

uint8_t* helper::mapped_file::end() {
    return &data_[size()];
}

uint8_t& helper::mapped_file::operator[](int index) {
    return data_[index];
}

helper::mapped_file::~mapped_file() {
    if (data_ != nullptr) {
        close();
    }
}

void helper::mapped_file::map(uint64_t size) {
    void* data = mmap(0, size, mmap_flags_, MAP_SHARED, fd_, 0);
    if (data == MAP_FAILED) {
        ::close(fd_);
        throw std::runtime_error(generate_error_message("Cannot map file"));
    }

    data_ = static_cast<uint8_t*>(data);
}

void helper::mapped_file::unmap() {
    if (munmap(data_, size()) < 0) {
        ::close(fd_);
        throw std::runtime_error(generate_error_message("Cannot unmap file"));
    }
}
