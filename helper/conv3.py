import argparse

import numpy as np
from PIL import Image

def from_image(in_file: str, out_file: str):
    image = np.array(Image.open(in_file))
    image = np.dstack((image, np.zeros((image.shape[0], image.shape[1]))))
    image = image.astype(np.uint8)
    w, h = image.shape[1], image.shape[0]
    size = np.array([w, h]).astype(np.int32)
    with open(out_file, 'wb') as f:
       f.write(size.tobytes())
       f.write(image.tobytes())


def to_image(in_file: str, out_file: str):
    size = np.fromfile(in_file, dtype=np.int32, count=2, offset=0)
    w, h = size[0], size[1]
    image = np.fromfile(in_file, dtype=np.uint8, offset=8)
    image = image.reshape((h, w, 4))
    image = image[:, :, :3]
    image = Image.fromarray(image)
    image.save(out_file)

parser = argparse.ArgumentParser()
parser.add_argument('-r', action='store_true',
        help='Converts binary file to image')
parser.add_argument('-i', type=str, required=True,
        help='Input file')
parser.add_argument('-o', type=str, required=True,
        help='Output file')

args = parser.parse_args()

in_file = args.i
out_file = args.o

if not args.r:
    from_image(in_file, out_file)
else:
    to_image(in_file, out_file)
