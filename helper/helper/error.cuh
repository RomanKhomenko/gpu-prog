#ifndef HELPER_ERROR_CUH_
#define HELPER_ERROR_CUH_

#include <cstdlib>
#include <iostream>

#define CUDA_CHECK_ERROR(err) { helper::cuda_check_error((err), __FILE__, __LINE__); }
#define CUDA_CHECK_LAST_ERROR { helper::cuda_check_last_error(__FILE__, __LINE__); }

namespace helper {

inline void cuda_check_error(cudaError_t err, const char* file, int line) {
    if (err != cudaSuccess) {
        std::cerr << "CUDA ERROR in " << file << ":" << line << ": "
                  << cudaGetErrorString(err)
                  << std::endl;
        std::exit(EXIT_FAILURE);
    }
}

inline void cuda_check_last_error(const char* file, int line) {
    cuda_check_error(cudaGetLastError(), file, line);
}

}

#endif // HELPER_ERROR_CUH_

