#ifndef HELPER_MAPPED_FILE_HPP_
#define HELPER_MAPPED_FILE_HPP_

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <stdexcept>
#include <cstdint>
#include <string>

namespace helper {

class mapped_file {
public:
    enum open_flags {
        ro = 0x0001,
        wo = 0x0010,
        rw = 0x0100,
        c  = 0x1000
    };

    mapped_file();

    void open(const std::string& name,
              uint64_t flags,
              uint64_t size);

    void open(int fd,
              uint64_t flags,
              uint64_t size);

    void close();

    uint64_t size() const;

    void remap(uint64_t size);

    uint8_t* begin();
   
    uint8_t* end();

    uint8_t& operator[](int index);

    ~mapped_file();

private:
    uint64_t parse_fd_flags(uint64_t flags);
    void map(uint64_t size);
    void unmap();

    uint8_t* data_;
    uint64_t size_;
    int fd_;
    uint64_t mmap_flags_;
};

}

#endif // HELPER_MAPPED_FILE_HPP_
