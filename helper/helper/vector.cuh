#ifndef HELPER_VECTOR_CUH_
#define HELPER_VECTOR_CUH_

#include <helper/error.cuh>

#include <inttypes.h>
#include <stdexcept>

namespace helper {

template<typename T>
class vector {
public:
    typedef uint64_t size_type;
    typedef T& reference;
    typedef const T& const_reference;
    typedef T* pointer;
    typedef const T* const_pointer;

    vector()
        : n_(0), data_(NULL) {}

    vector(size_type n)
        : n_(n), data_(allocate(n)) {}

    vector(size_type n, const_pointer data) { 
        n_ = n;
        data_ = allocate(n);
        from_host(data);
    }

    __host__ __device__
    inline size_type size() const { return n_; }
    
    __host__ __device__
    inline bool empty() const { return n_ == 0; }

    __device__
    inline reference operator[](const int i) { return data_[i]; }

    __device__
    inline const_reference operator[](const int i) const { return data_[i]; }

    inline pointer data() { return data_; }
    inline const_pointer data() const { return data_; }

    inline pointer begin() { return data_; }
    inline pointer end() { return &data_[size()]; }

    void from_host(const_pointer data) {
        if (empty()) {
            throw std::runtime_error("copy to empty vector");
        }

        cudaError_t err = cudaMemcpy(data_, data, sizeof(T) * size(), cudaMemcpyHostToDevice);
        CUDA_CHECK_ERROR(err);
    }

    void to_host(pointer data) const {
        if (empty()) {
            throw std::runtime_error("copy from empty vector");
        }

        cudaError_t err = cudaMemcpy(data, data_, sizeof(T) * size(), cudaMemcpyDeviceToHost);
        CUDA_CHECK_ERROR(err);
    }

    void free() {
        deallocate(data_);
        n_ = 0;
        data_ = NULL;
    }

    virtual ~vector() {
        n_ = 0;
        data_ = NULL;
    }
        
private:
    static pointer allocate(size_type n) {
        T* mem;

        cudaError_t err = cudaMalloc(&mem, sizeof(T) * n);
        CUDA_CHECK_ERROR(err);

        return mem;
    }

    static void deallocate(pointer data) {
        cudaError_t err = cudaFree(data);
        CUDA_CHECK_ERROR(err);
    }

    size_type n_;
    pointer data_;
};

}

#endif // HELPER_VECTOR_CUH_

