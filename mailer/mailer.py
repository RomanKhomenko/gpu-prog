#!/usr/bin/env python3

import smtplib
import ssl
import getpass
import json
import os
import argparse

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart

parser = argparse.ArgumentParser(description="Mailer: send smth to smb")
parser.add_argument('-t', '--to', type=str, help="to address")
parser.add_argument('-f', '--from', type=str, help="from address")
parser.add_argument('-s', '--subject', type=str, help="mail subject")
parser.add_argument('-a', '--attachments', nargs='+', type=str, help="attachments")
args = vars(parser.parse_args())

port = 587

send_email = args["from"]
recv_email = args["to"]
subject = args["subject"]

script_dir = os.path.dirname(os.path.realpath(__file__))
cred_file = os.path.join(script_dir, ".cred.json")
with open(cred_file) as json_file:
    cred = json.load(json_file)

files = args["attachments"]


message = MIMEMultipart()
message["From"] = send_email
message["To"] = recv_email
message["Subject"] = subject
message["Bcc"] = recv_email
message.preamble = "You will not see this in a MIME-aware mail reader.\n"

for f in files:
    with open(f, "rb") as attchmnt:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attchmnt.read())
        encoders.encode_base64(part)
        part.add_header("Content-Disposition", f"attachment; filename= {f}")
        message.attach(part)
        
text = message.as_string()

context = ssl.create_default_context()
with smtplib.SMTP("smtp.gmail.com", port) as server:
    server.ehlo()
    server.starttls()
    server.login(send_email, cred["key"])
    server.ehlo()
    server.sendmail(send_email, recv_email, text)

