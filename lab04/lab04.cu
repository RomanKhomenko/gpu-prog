#include <iostream>
#include <iomanip>
#include <vector>

#include <helper/error.cuh>

#include <thrust/device_vector.h>
#include <thrust/extrema.h>

#ifdef DEBUG
#include "../helper/cuPrintf.cu"
#include <eigen3/Eigen/Dense>

namespace eig = Eigen;

using matrix = eig::Matrix<double, eig::Dynamic, eig::Dynamic>;
using map_matrix = eig::Map<matrix>;

#endif

namespace th = thrust;

__device__ __host__
inline int index(int i, int j, int n) {
    return j * n + i;
}

__global__ void swap(double* m, int x, int y, int n) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int offset = gridDim.x * blockDim.x;

    for (int j = idx; j < n; j += offset) {
        double tmp = m[index(x, j, n)];
        m[index(x, j, n)] = m[index(y, j, n)];
        m[index(y, j, n)] = tmp;
    }
}

__global__ void add_rows(double* m, double* o, int n, int row) {
    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    int idy = threadIdx.y + blockDim.y * blockIdx.y;
    int offsetx = blockDim.x * blockDim.x;
    int offsety = blockDim.y * blockDim.y;

    const double k2 = 1 / m[index(row, row, n)];

    if (idx == 0 && idy == 0) {
        o[index(row, row, n)] = m[index(row, row, n)];
    }


    for (int i = idx; i < n; i += offsetx) {
        const double c = k2 * m[index(i, row, n)];
        if (i > row) {
            o[index(i, row, n)] = c;
            for (int j = idy; j < n; j += offsety) {
                if (j > row) {
                    o[index(i, j, n)] = m[index(i, j, n)] - c * m[index(row, j, n)];
                }
           }
        }
        if (row > 0 && i > row) {
            o[index(i, row - 1, n)] = m[index(i, row - 1, n)];
        }
    }

    if (row > 0) {
        for (int j = idy; j < n; j += offsety) {
            o[index(row, j, n)] = m[index(row, j, n)];
        }
    }
}

struct comparator {
    __host__ __device__
    bool operator()(double left, double right) {
        return fabs(left) < fabs(right);
    }
};

int main() {
    int n{};

    std::ios::sync_with_stdio(false);

    std::cin >> n;

    std::vector<int> host_p(n);
    std::vector<double> host_arr(n * n);
    th::device_vector<double> dev_arr(n * n);
    th::device_vector<double> dev_arr2(n * n, 0);

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            std::cin >> host_arr[index(i, j, n)];
        }
    }

#ifndef DEBUG
    std::cerr << "Dim: " << n << std::endl;
    for (int i = 0; i < std::min(n, 100); i++) {
        for (int j = i; j < std::min(n, i + 11); j++) {
            std::cerr << host_arr[index(i, j, n)] << " ";
        }
        std::cerr << std::endl;
    }
    std::cerr << std::endl;
#endif

    th::copy(std::begin(host_arr), std::end(host_arr), std::begin(dev_arr));
    th::copy(std::begin(host_arr), std::end(host_arr), std::begin(dev_arr2));

#ifdef DEBUG
    cudaPrintfInit();
#endif
    
    auto m_ptr = &dev_arr;
    auto o_ptr = &dev_arr2;

    for (int i = 0; i < n - 1; i++) {
        auto begin = std::begin(*m_ptr) + index(i, i, n);
        auto end = begin + n - i;

        auto iter = th::max_element(begin, end, comparator{});
        auto swap_index = iter - begin + i;

        if (swap_index > i) {
            host_p[i] = swap_index;
            swap<<< 256, 256 >>>(th::raw_pointer_cast(m_ptr->data()),
                                 i, swap_index, n);        
            CUDA_CHECK_LAST_ERROR;

            swap<<< 256, 256 >>>(th::raw_pointer_cast(o_ptr->data()),
                                 i, swap_index, n);
            CUDA_CHECK_LAST_ERROR;

        }
        else {
            host_p[i] = i;
        }

        add_rows<<< dim3(32, 32), dim3(32, 32) >>>(th::raw_pointer_cast(m_ptr->data()),
                                                   th::raw_pointer_cast(o_ptr->data()),
                                                   n, i);
       
        CUDA_CHECK_LAST_ERROR;

        std::swap(m_ptr, o_ptr);
    }

    host_p[n - 1] = n - 1;

#ifdef DEBUG
    cudaPrintfDisplay();
    cudaPrintfEnd();
#endif
   
#ifdef DEBUG
    auto m = map_matrix(th::raw_pointer_cast(host_arr.data()), n, n);
        
    eig::PartialPivLU<matrix> lu_decomp(m);
    eig::FullPivLU<matrix> flu_decomp(m);

    if (!flu_decomp.isInvertible()) {
        std::cout << "Bad matrix sample!" << std::endl;
    }
    
    auto lu = lu_decomp.matrixLU();

    th::copy(std::begin(*m_ptr), std::end(*m_ptr), std::begin(host_arr));

    auto result = map_matrix(th::raw_pointer_cast(host_arr.data()), n, n);

    auto norm = (lu - result).norm();
    std::cout << norm << std::endl;
    if (norm > 1e-6) {
        std::cout << lu << std::endl;
        std::cerr << result << std::endl;
    }
#else
    th::copy(std::begin(*m_ptr), std::end(*m_ptr), std::begin(host_arr));

    std::cout << std::scientific << std::setprecision(10);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            std::cout << host_arr[index(i, j, n)] << " ";
        }
        std::cout << std::endl;
    }

    for (int i = 0; i < n; i++) {
        std::cout << host_p[i] << " ";
    }
    std::cout << std::endl;
#endif
}
