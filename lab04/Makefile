NVCC=nvcc
NVFLAGS=--std c++11 --compiler-options "-Wall -Wextra -std=c++11 -I../helper" -c

HELPER=../helper/helper/error.cuh

CUDA_SOURCES=lab04.cu
CUDA_OBJECTS=$(CUDA_SOURCES:.cu=.o)
TARGET=a.out
ATTACHMENT=lab04_attachment.cu
ATTACHMENT_SIGN=lab04_attachment.cu.asc
REPORT_SOURCES=report-lab04.tex
REPORT=$(REPORT_SOURCES:.tex=.pdf)
REPORT_SIGN=report-lab04.pdf.asc

MAILER=../mailer/mailer.py
MAILER_FLAGS=-f romankhomenko1995@gmail.com -t checkpgp@gmail.com -s "pgp:4"

all: $(CUDA_SOURCES) $(TARGET)

debug: NVFLAGS += -g -DDEBUG
debug: $(CUDA_SOURCES) $(TARGET)

$(TARGET): $(CUDA_OBJECTS)
	$(NVCC) $(LDFLAGS) $(CUDA_OBJECTS) -o $@

%.o: %.cu
	$(NVCC) $(NVFLAGS) $< -o $@

send: $(ATTACHMENT) $(ATTACHMENT_SIGN)
	$(MAILER) $(MAILER_FLAGS) -a $(ATTACHMENT) $(ATTACHMENT_SIGN)

send_report: $(REPORT) $(REPORT_SIGN)
	$(MAILER) $(MAILER_FLAGS) -a $(REPORT) $(REPORT_SIGN)

sign: $(ATTACHMENT_SIGN)

$(ATTACHMENT_SIGN): $(ATTACHMENT)
	$(shell gpg -ab $(ATTACHMENT))

$(ATTACHMENT): $(HELPER) $(CUDA_SOURCES)
	$(shell cat $(HELPER) $(CUDA_SOURCES) | sed -e "/#include\s*<helper/d" > $(ATTACHMENT))

report_sign: $(REPORT_SIGN)

report: $(REPORT)

$(REPORT_SIGN): $(REPORT)
	gpg -ab $(REPORT)

%.pdf: %.tex
	latexmk -pdf
	
clean:
	$(RM) $(CUDA_OBJECTS) $(TARGET) $(ATTACHMENT) $(ATTACHMENT_SIGN)
	$(RM) $(REPORT_SIGN)
	$(shell latexmk -C)

