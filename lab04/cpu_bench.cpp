#include <algorithm>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <vector>
#include <thread>
#include <random>

inline int index(int i, int j, int n) {
    return j * n + i;
}

void swap(double* m, int x, int y, int n, int thread_id, int thread_count) {
    uint64_t offset = n / thread_count + (n % thread_count == 0 ? 0 : 1);
    uint64_t offset_current = offset;
    if (n - offset * thread_id < offset) {
        offset_current  = n - offset * thread_id;
    }

    for (int j = thread_id * offset; j < thread_id * offset + offset_current; j++) {
        double tmp = m[index(x, j, n)];
        m[index(x, j, n)] = m[index(y, j, n)];
        m[index(y, j, n)] = tmp;
    }
}

void run_swap(double* m, int x, int y, int n, int thread_count) {
    std::vector<std::thread> threads;

    for (int thread_id = 0; thread_id < thread_count; thread_id++) {
        threads.push_back(std::thread(swap, m, x, y, n, thread_id, thread_count));
    }

    for (auto& thread : threads) {
        thread.join();
    }
}

void add_rows(double* m, double* o, int n, int row, int thread_id, int thread_count) {
    uint64_t offset = n / thread_count + (n % thread_count == 0 ? 0 : 1);
    uint64_t offset_current = offset;
    if (n - offset * thread_id < offset) {
        offset_current  = n - offset * thread_id;
    } 

    const double k2 = 1 / m[index(row, row, n)];

    for (int i = thread_id * offset; i < thread_id * offset + offset_current; i++) {
        const double c = k2 * m[index(i, row, n)];
        if (i > row) {
            o[index(i, row, n)] = c;
            for (int j = 0; j < n; j++) {
                if (j > row) {
                    o[index(i, j, n)] = m[index(i, j, n)] - c * m[index(row, j, n)];
                }
           }
        }
        if (row > 0 && i > row) {
            o[index(i, row - 1, n)] = m[index(i, row - 1, n)];
        }
    }

    if (row > 0) {
        for (int j = thread_id * offset; j < thread_id * offset + offset_current; j++) {
            o[index(row, j, n)] = m[index(row, j, n)];
        }
    }
}

void run_add_rows(double* m, double* o, int n, int row, int thread_count) {
    std::vector<std::thread> threads;

    o[index(row, row, n)] = m[index(row, row, n)];

    for (int thread_id = 0; thread_id < thread_count; thread_id++) {
        threads.push_back(std::thread(add_rows, m, o, n, row, thread_id, thread_count));
    }

    for (auto& thread : threads) {
        thread.join();
    }
}

struct comparator {
    bool operator()(double left, double right) {
        return std::abs(left) < std::abs(right);
    }
};

int main() {
    int n{};

    std::ios::sync_with_stdio(false);

    std::cin >> n;

    std::vector<int> host_p(n);
    std::vector<double> host_arr(n * n);
    std::vector<double> dev_arr1(n * n);
    std::vector<double> dev_arr2(n * n);

    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(-10, 10);

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
             host_arr[index(i, j, n)] = dis(gen);
        }
    }
   
    std::copy(std::begin(host_arr), std::end(host_arr), std::begin(dev_arr1));
    std::copy(std::begin(host_arr), std::end(host_arr), std::begin(dev_arr2));

    auto m_ptr = &dev_arr1;
    auto o_ptr = &dev_arr2;

    for (int thread_count = 1; thread_count <= 7; thread_count++) {
    int ellapsed_seconds = 0;
    for (int t = 0; t < 5; t++) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    for (int i = 0; i < n - 1; i++) {
        auto begin = std::begin(*m_ptr) + index(i, i, n);
        auto end = begin + n - i;

        auto iter = std::max_element(begin, end, comparator{});
        auto swap_index = iter - begin + i;

        if (swap_index > i) {
            host_p[i] = swap_index;
            run_swap(m_ptr->data(), i, swap_index, n, thread_count);        
            run_swap(o_ptr->data(), i, swap_index, n, thread_count);
        }
        else {
            host_p[i] = i;
        }

        run_add_rows(m_ptr->data(), o_ptr->data(), n, i, thread_count);

        std::swap(m_ptr, o_ptr);
    }

    host_p[n - 1] = n - 1;
   
    end = std::chrono::system_clock::now();
    ellapsed_seconds += std::chrono::duration_cast<std::chrono::microseconds>
                                (end-start).count();
    std::cerr << ellapsed_seconds << std::endl;

    std::copy(std::begin(host_arr), std::end(host_arr), std::begin(dev_arr1));
    std::copy(std::begin(host_arr), std::end(host_arr), std::begin(dev_arr2));
    }
    std::cerr << "Threads " << thread_count << " time " <<  ellapsed_seconds / 5.0 << std::endl;
    }    

    std::cout << std::scientific << std::setprecision(10);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            std::cout << (*o_ptr)[index(i, j, n)] << " ";
        }
        std::cout << std::endl;
    }

    for (int i = 0; i < n; i++) {
        std::cout << host_p[i] << " ";
    }
    std::cout << std::endl;
}
