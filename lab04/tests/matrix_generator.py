import numpy as np
import scipy.linalg as linalg

def print_matrix(a, n):
    for i in range(n):
        for j in range(n):
            print(f'{a[i][j]:>2.5e}', end=' ')
        print('')

N = 40

a = np.random.rand(N, N)

print(N)
print_matrix(a, N)

p, l, u = linalg.lu(a)

print("P matrix")
print_matrix(p, N)
print("L matrix")
print_matrix(l, N)
print("U matrix")
print_matrix(u, N)
