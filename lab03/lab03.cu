#include <helper/error.cuh>
#include <helper/mapped_file.hpp>
#include <helper/vector.cuh>

#ifdef DEBUG
#include "../helper/cuPrintf.cu"
#endif

#include <cmath>
#include <chrono>
#include <iostream>
#include <vector>

namespace h = helper;

inline float3 operator+(const float3& f3, const uchar4& u4) {
    return make_float3(f3.x + u4.x, f3.y + u4.y, f3.z + u4.z);
}

inline float3 operator/(const float3& f3, float c) {
    return make_float3(f3.x / c, f3.y / c, f3.z / c);
}

__device__ 
inline float operator*(const uchar4& u4, const float3& f3) {
    return u4.x * f3.x + u4.y * f3.y + u4.z * f3.z;
}

template<class T>
inline float norm(const T& t) {
    return sqrt(t.x * t.x + t.y * t.y + t.z * t.z);
}

template<class T>
__device__ inline float norm_dev(const T& t) {
    return sqrtf(t.x * t.x + t.y * t.y + t.z * t.z);
}

__constant__ float3 g_avgs[32];
__constant__ float g_avg_norms[32];

__global__ void spectrum_method(uint64_t w,
                                uint64_t h,
                                uint64_t nc,
                                h::vector<uchar4> v) {
    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    int idy = threadIdx.y + blockDim.y * blockIdx.y;
    int offsetx = blockDim.x * blockDim.x;
    int offsety = blockDim.y * blockDim.y;    

    for (int i = idx; i < w; i += offsetx) {
        for (int j = idy; j < h; j += offsety) {
            int cls = 0x100;
            float max_val = -1.0;

            auto& p = v[j * w + i];
            auto n = norm_dev(p);
            for (int k = 0; k < nc; k++) {
                float val = (p * g_avgs[k]) / g_avg_norms[k];

#ifdef DEBUG
                cuPrintf("%d, %d k = %d, i = %f, imax = %f\n",
                         i, j, k, val, max_val);
#endif
 
                if (abs(max_val - val) < 1e-6) {
                   cls = min(cls, k);
                }
                else {
                    if (max_val < val) {
                        max_val = val;
                        cls = k;
                    }
                } 
            }
            
            p.w = cls;
        }
    }
}

#ifdef DEBUG

static inline std::ostream& operator<<(std::ostream& os, const uchar4& t) {
    os << (int)t.x << ", " <<
          (int)t.y << ", " <<
          (int)t.z;
    return os;
}

#endif

int main() {
    using of = h::mapped_file::open_flags;

    std::ios::sync_with_stdio(false);

    cudaError_t err;

    std::string file_name_in;
    std::string file_name_out;

    std::getline(std::cin, file_name_in);
    std::getline(std::cin, file_name_out);

    h::mapped_file fin;
    fin.open(file_name_in, of::ro, 2 * sizeof(uint32_t));

    uint32_t* size_ptr = reinterpret_cast<uint32_t*>(std::begin(fin));
    uint32_t w = *size_ptr++;
    uint32_t h = *size_ptr;

    uint64_t file_size = 2 * sizeof(uint32_t) + w * h * sizeof(uchar4);
    fin.remap(file_size);

    uchar4* data_in = reinterpret_cast<uchar4*>(std::begin(fin) + 2 * sizeof(uint32_t));

    int nc;
    int np;
    std::vector<float3> avgs;
    std::vector<float> norm_avgs;

    std::cin >> nc;
    for (int j = 0; j < nc; j++) {
        float3 avgj = make_float3(0, 0, 0);
        int x;
        int y;

        std::cin >> np;
        for (int i = 0; i < np; i++) {
            std::cin >> x >> y;

#ifdef DEBUG
            std::cout << x  << ", " << y << ": " << data_in[x * w + y] << std::endl;      

            if (x > w - 1 || y > h - 1) {
                throw std::runtime_error("bad coordinates");
            }
#endif

            avgj = avgj + data_in[y* w + x];
        }

        avgj = avgj / np;

#ifdef DEBUG
        std::cout << avgj.x << ", " << avgj.y << ", " << avgj.z << std::endl;
#endif

        avgs.push_back(avgj);
        norm_avgs.push_back(norm(avgj));
    }

    err = cudaMemcpyToSymbol(g_avgs, avgs.data(), avgs.size() * sizeof(float3),
                             0, cudaMemcpyHostToDevice);
    CUDA_CHECK_ERROR(err);

    err = cudaMemcpyToSymbol(g_avg_norms, norm_avgs.data(), avgs.size() * sizeof(float),
                             0, cudaMemcpyHostToDevice);

    auto dev_arr_out = h::vector<uchar4>(w * h, data_in);
 
#ifdef DEBUG
    cudaPrintfInit();
#endif

    for (int i = 8; i < 512; i *= 2) {
        for (int j = 8; j < 64; j *= 2) {
            int ellapsed_seconds = 0;
            for (int k = 0; k < 5; k++) {
                auto start = std::chrono::system_clock::now();

                spectrum_method<<< dim3(i, i), dim3(j, j) >>>(w, h, nc, dev_arr_out);
                cudaDeviceSynchronize();

                auto end = std::chrono::system_clock::now();

                CUDA_CHECK_LAST_ERROR;

                ellapsed_seconds += std::chrono::duration_cast<std::chrono::microseconds>
                    (end - start).count();

            }
            std::cout << i << ", " << j << ": " << ellapsed_seconds / 5.0 << std::endl;
        }
    }

#ifdef DEBUG    
    cudaPrintfDisplay();
    cudaPrintfEnd();
#endif

    h::mapped_file fout;
    fout.open(file_name_out, of::rw | of::c, file_size);

    size_ptr = reinterpret_cast<uint32_t*>(std::begin(fout));
    *size_ptr++ = w;
    *size_ptr = h;

    uchar4* data_out = reinterpret_cast<uchar4*>(std::begin(fout) + 2 * sizeof(uint32_t));
    dev_arr_out.to_host(data_out);

    fin.close();
    fout.close();
}
