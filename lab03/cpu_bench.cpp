#include <helper/mapped_file.hpp>

#ifdef DEBUG
#include "../helper/cuPrintf.cu"
#endif

#include <cmath>
#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
#include <cstring>

namespace h = helper;

struct float3 {
    float x;
    float y;
    float z;
};

struct uchar4 {
    uint8_t x;
    uint8_t y;
    uint8_t z;
    uint8_t w;
};

inline float3 make_float3(float x, float y, float z) {
    return float3 {x, y, z};
}


inline float3 operator+(const float3& f3, const uchar4& u4) {
    return make_float3(f3.x + u4.x, f3.y + u4.y, f3.z + u4.z);
}

inline float3 operator/(const float3& f3, float c) {
    return make_float3(f3.x / c, f3.y / c, f3.z / c);
}

inline float operator*(const uchar4& u4, const float3& f3) {
    return u4.x * f3.x + u4.y * f3.y + u4.z * f3.z;
}

template<class T>
inline float norm(const T& t) {
    return sqrt(t.x * t.x + t.y * t.y + t.z * t.z);
}

template<class T>
inline float norm_dev(const T& t) {
    return sqrtf(t.x * t.x + t.y * t.y + t.z * t.z);
}

float3 g_avgs[32];
float g_avg_norms[32];

void spectrum_method(uint64_t w,
                     uint64_t h,
                     uint64_t nc,
                     uint64_t thread_id,
                     uint64_t thread_count,
                     uchar4* v) {
    
    uint64_t offset = w / thread_count + (w % thread_count == 0 ? 0 : 1);
    uint64_t offset_current = offset;
    if (w - offset * thread_id < offset) {
        offset_current  = w - offset * thread_id;
    }

    for (int i = thread_id * offset; i < thread_id * offset + offset_current; i++) {
        for (int j = 0; j < h; j++) {
            int cls = 0x100;
            float max_val = -1.0;

            auto& p = v[j * w + i];
            auto n = norm_dev(p);
            for (int k = 0; k < nc; k++) {
                float val = (p * g_avgs[k]) / g_avg_norms[k];

#ifdef DEBUG
                cuPrintf("%d, %d k = %d, i = %f, imax = %f\n",
                         i, j, k, val, max_val);
#endif
 
                if (abs(max_val - val) < 1e-6) {
                   cls = std::min(cls, k);
                }
                else {
                    if (max_val < val) {
                        max_val = val;
                        cls = k;
                    }
                } 
            }
            
            p.w = cls + 1;
        }
    }
}

#ifdef DEBUG

static inline std::ostream& operator<<(std::ostream& os, const uchar4& t) {
    os << (int)t.x << ", " <<
          (int)t.y << ", " <<
          (int)t.z;
    return os;
}

#endif

int main() {
    using of = h::mapped_file::open_flags;

    std::ios::sync_with_stdio(false);

    std::string file_name_in;
    std::string file_name_out;

    std::getline(std::cin, file_name_in);
    std::getline(std::cin, file_name_out);

    h::mapped_file fin;
    fin.open(file_name_in, of::rw, 2 * sizeof(uint32_t));

    uint32_t* size_ptr = reinterpret_cast<uint32_t*>(std::begin(fin));
    uint32_t w = *size_ptr++;
    uint32_t h = *size_ptr;

    uint64_t file_size = 2 * sizeof(uint32_t) + w * h * sizeof(uchar4);
    fin.remap(file_size);

    uchar4* data_in = reinterpret_cast<uchar4*>(std::begin(fin) + 2 * sizeof(uint32_t));

    int nc;
    int np;
    std::vector<float3> avgs;
    std::vector<float> norm_avgs;

    std::cin >> nc;
    for (int j = 0; j < nc; j++) {
        float3 avgj = make_float3(0, 0, 0);
        int x;
        int y;

        std::cin >> np;
        for (int i = 0; i < np; i++) {
            std::cin >> x >> y;

#ifdef DEBUG
            std::cout << x  << ", " << y << ": " << data_in[x * w + y] << std::endl;      

            if (x > w - 1 || y > h - 1) {
                throw std::runtime_error("bad coordinates");
            }
#endif

            avgj = avgj + data_in[y* w + x];
        }

        avgj = avgj / np;

#ifdef DEBUG
        std::cout << avgj.x << ", " << avgj.y << ", " << avgj.z << std::endl;
#endif

        avgs.push_back(avgj);
        norm_avgs.push_back(norm(avgj));
    }

    std::copy(std::begin(avgs), std::end(avgs), g_avgs);
    std::copy(std::begin(norm_avgs), std::end(norm_avgs), g_avg_norms);
 
#ifdef DEBUG
    cudaPrintfInit();
#endif

    std::vector<std::thread> threads;
    int thread_count = 10;

    for (int i = 1; i < thread_count + 1; i++) {
        int ellapsed_seconds = 0;
        for (int j = 0; j < 5; j++) {
            std::chrono::time_point<std::chrono::system_clock> start, end;
            start = std::chrono::system_clock::now();

            for (int thread_id = 0; thread_id < i; thread_id++) {
                 threads.push_back(std::thread(spectrum_method,
                                               h, w, nc,
                                               thread_id, i,
                                               data_in));
            }

            for (auto& thread : threads) {
                thread.join();
            }

            end = std::chrono::system_clock::now();
            ellapsed_seconds += std::chrono::duration_cast<std::chrono::microseconds>
                                 (end-start).count();

            threads.clear();
        }
        std::cerr << i << ": " << ellapsed_seconds / 5.0 << std::endl;
    }

#ifdef DEBUG    
    cudaPrintfDisplay();
    cudaPrintfEnd();
#endif

    h::mapped_file fout;
    fout.open(file_name_out, of::rw | of::c, file_size);

    size_ptr = reinterpret_cast<uint32_t*>(std::begin(fout));
    *size_ptr++ = w;
    *size_ptr = h;

    uchar4* data_out = reinterpret_cast<uchar4*>(std::begin(fout) + 2 * sizeof(uint32_t));
    std::memcpy(data_out, data_in, w * h * sizeof(uchar4));

    fin.close();
    fout.close();
}
