#include <helper/error.cuh>
#include <helper/mapped_file.hpp>

#include <thrust/device_vector.h>

#include <algorithm>
#include <iostream>
#include <cstdio>

namespace h = helper;
namespace th = thrust;
using of = h::mapped_file::open_flags;

const uint64_t COUNT = 256;
const uint64_t BSIZE = 256;
const uint64_t WSIZE = 32;

__global__
void hist(const uint8_t* x, uint32_t* h, uint64_t n) {
    __shared__ uint32_t s[COUNT + COUNT / WSIZE + 1];

    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int offset = gridDim.x * blockDim.x;

    for (int i = threadIdx.x; i < COUNT; i += blockDim.x) {
        s[i] = 0;
    }

    __syncthreads();

    for (int i = idx; i < n; i += offset) {
        atomicAdd(&s[x[i] + x[i] / 32], 1);
    }

    __syncthreads();

    for (int i = threadIdx.x; i < COUNT; i += blockDim.x) {
        atomicAdd(&h[i], s[i + i / WSIZE]);
    }
}

__global__
void scan(const uint32_t* x, uint32_t* psum, uint64_t n) {
    __shared__ uint32_t s[2 * BSIZE];

    int idx = threadIdx.x;
    int offset = 1;

    s[2 * idx] = x[2 * idx];
    s[2 * idx + 1] = x[2 * idx + 1];

    auto index1 = [](int offset, int idx) {
        return offset * (2 * idx + 1) - 1;
    };

    auto index2 = [](int offset, int idx) {
        return offset * (2 * idx + 2) - 1;
    };

    for (int level = n / 2; level > 0; level /= 2) {
        __syncthreads();
    
        if (idx < level) {
            int idx1 = index1(offset, idx);
            int idx2 = index2(offset, idx);

            s[idx2] += s[idx1];
        }

        offset *= 2;
    }

    if (idx == 0) {
        s[n - 1] = 0;
    }

    for (int level = 1; level < n; level *= 2) {
        offset /= 2;

        __syncthreads();

        if (idx < level) {
            int idx1 = index1(offset, idx);
            int idx2 = index2(offset, idx);

            uint32_t tmp = s[idx1];
            
            s[idx1] = s[idx2];
            s[idx2] += tmp;
        }
    }

    __syncthreads();

    if (2 * idx < n) {
        psum[2 * idx] = s[2 * idx];
    }

    if (2 * idx + 1 < n) {
        psum[2 * idx + 1] = s[2 * idx + 1];
    }
}

__global__
void make_result(uint32_t* psum, uint8_t* x, uint32_t n) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int offset = gridDim.x * blockDim.x;

    for (int i = idx; i < n; i += offset) {
        int current = psum[i];
        int next = psum[i + 1];

        int lenght = next - current;

        while (lenght > 0) {
            x[current] = i;
            current++;
            lenght--;
        }
    }
}

int main() {
    h::mapped_file fin;

    fin.open(STDIN_FILENO, of::ro, sizeof(uint32_t));

    auto size_ptr = reinterpret_cast<uint32_t*>(std::begin(fin));
    auto n = *size_ptr;

    if (n == 0) {
        return 0;
    }

    uint64_t file_size = n * sizeof(uint8_t) + sizeof(uint32_t);
    fin.remap(file_size);

    std::cerr << n << ": ";
    for (uint64_t i = 0; i < std::min((uint32_t)250, n); i++) {
        std::cerr << static_cast<int>(*(std::begin(fin) + sizeof(uint32_t) + i))
                  << ",";
    }
    std::cerr << std::endl;

    th::device_vector<uint8_t> x(n);
    th::device_vector<uint32_t> h(COUNT, 0);
    th::device_vector<uint32_t> psum(COUNT + 1, 0);
    std::vector<uint8_t> buffer(n);

    if (n > 0) {
        th::copy(std::begin(fin) + sizeof(uint32_t), std::end(fin), std::begin(x));

        hist<<< 16, 256 >>>(th::raw_pointer_cast(x.data()),
                            th::raw_pointer_cast(h.data()),
                            n);
        cudaDeviceSynchronize();
        CUDA_CHECK_LAST_ERROR;
    
        scan<<< 1, BSIZE >>>(th::raw_pointer_cast(h.data()),
                             th::raw_pointer_cast(psum.data()),
                             COUNT);
        cudaDeviceSynchronize();
        CUDA_CHECK_LAST_ERROR;

        auto value = h[COUNT - 1];
        if (value != 0) {
            psum[COUNT] = psum[COUNT - 1] + value;
        }

        make_result<<< 1, 256 >>>(th::raw_pointer_cast(psum.data()),
                                  th::raw_pointer_cast(x.data()),
                                  COUNT);
        CUDA_CHECK_LAST_ERROR;
        th::copy(std::begin(x), std::end(x), std::begin(buffer));
    }
    else {
        buffer[0] = *(std::begin(fin) + sizeof(uint32_t));
    }
    
    write(STDOUT_FILENO, buffer.data(), buffer.size() * sizeof(uint8_t));

#ifdef DEBUG
    for (int i = 0; i < COUNT; i++) {
        std::cerr << i << ": " << (int)h[i] << std::endl;
    }

    for (int i = 0; i < n; i++) {
    //    std::cerr << static_cast<int>(buffer[i]) << " ";
    }
    std::cerr << std::endl;
#endif
}
