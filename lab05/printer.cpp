#include <iostream>
#include <cstdint>
#include <fstream>
#include <string>
#include <stdexcept>

int main(int argc, char** argv) {
    if (argc < 2) {
        throw std::runtime_error("bad_arguments");
    }

    uint32_t size = 0;
    std::string path(argv[1]);

    std::ifstream fs(path, std::ios::in | std::ios::binary);

    fs.read(reinterpret_cast<char*>(&size), sizeof(size));
    std::cout << size << std::endl;

    for (int i = 0; i < size; i++) {
        uint8_t tmp;
        fs.read(reinterpret_cast<char*>(&tmp), sizeof(tmp));
        std::cout << (int)tmp << " ";
    }
    std::cout << std::endl;
}
