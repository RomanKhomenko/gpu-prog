\documentclass[a4paper,14pt]{extarticle}
\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english,russian]{babel}
\usepackage{indentfirst}
\usepackage{amsmath}
\usepackage{geometry}

\geometry{left=1cm}
\geometry{right=1cm}
\geometry{top=1cm}
\geometry{bottom=1.5cm}

\usepackage{graphicx}
\usepackage[normalem]{ulem}
\usepackage{hyperref}
\usepackage{textcomp}
\usepackage{listings}
\usepackage{diagbox}
\usepackage{algorithm}
\usepackage{algpseudocode}

\DeclareMathOperator*{\argmax}{argmax}

\begin{document}
	
\lstdefinestyle{clang}{
	language=C,
	basicstyle=\footnotesize,
    frame=L
}

\begin{titlepage}
\begin{center}
    {МОСКОВСКИЙ АВИАЦИОННЫЙ ИНСТИТУТ} \\
    {(НАЦИОНАЛЬНЫЙ ИССЛЕДОВАТЕЛЬСКИЙ УНИВЕРСИТЕТ)} \\
    \vspace{0.5cm}
    {Факультет <<Информационные технологии и прикладная математика>>} \\
    {Кафедра <<Вычислительная математика и программирование>>}

\vspace{4cm}

\bf{
    {Лабораторная работа №5} \\
    {по курсу <<Программирование графических процессоров>>} \\
}

\vspace{2cm}

\bf{
    {Сортировка чисел на GPU. Свертка, сканирование, гистограмма.} \\
}

\large

\end{center}

\vspace{6cm}
\begin{flushright}
\begin{minipage}{0.5\textwidth}
    \begin{flushleft}
        \begin{tabular}{l}
            {Выполнил: Р.~Д.~Хоменко} \\
            {Группа: 8О-408Б} \\
        \end{tabular}
       
        \begin{tabular}{ll}
            Преподаватели: & К.~Г.~Крашенинников \\
                           & A.~Ю.~Морозов \\
        \end{tabular}
    \end{flushleft}
\end{minipage}
\end{flushright}

\vfill
\begin{center}
    {Москва, 2019}
\end{center}

\end{titlepage}

\section{Условие}
\subsection{Цель работы}
Ознакомление с фундаментальными алгоритмами GPU: свертка
(reduce), сканирование (blelloch scan) и гистограмма (histogram).
Реализация одной из сортировок на CUDA.
Использование разделяемой и других видов памяти.

\subsection{Вариант 3}
Требуется реализовать сортировку подсчетом для чисел типа uchar.
Должны быть реализованы:
\begin{itemize}
    \item Алгоритм гистограммы, с использованием атомарных операций и
          разделяемой памяти.
    \item Алгоритм сканирования, с бесконфликтным использованием
          разделяемой памяти.
\end{itemize}
Ограничения: $n \leq 537 \cdot 10^6$

\section{Программное и аппаратное обеспечение}
Intel(R) Core(TM) i5-8500 CPU @ 3.00GHz, 16Gb RAM,
512Gb SSD, 1Tb HDD.

GPGPU -- GeForce GTX 1060, чип -- GP106, compute capability -- 6.1,
глобальная память -- 6Gb, разделяемая память на блок -- 48Kb,
максимальный размер блока -- $1024 \times 1024 \times 64$,
количество регистров на блок -- 65536,
максимальное число потоков на блок -- 1024,
максимальный размер сетки -- $2147483647 \times 65535 \times 65535$,
количество мультипроцессоров -- 10.

Debian 4.19.16-1 (2019-01-17) x86\_64 GNU/Linux,
CUDA 10: nvcc, nvprof, cuda-gdb, cuda-memcheck,
Vim 8.1.
    
\section{Метод решения}
\textbf{Гистограмма}. Для реализации гистограммы будем использовать
разделяемую память.
Для предотвращения конфликтов банков памяти, будем
каждым 32 элементам (размер варпа) добавлять фиктивный.
Каждый поток в блоке атомарными операциями будет инкрементировать
счетчик в разделяемой памяти для данного числа.
Далее следует атомарное прибавление значений из разделяемой памяти
к значениям в глобальной памяти. 

\textbf{Скан}. Алгоритм состоит из двух этапов:
\begin{enumerate}
    \item Построение дерева частичных сумм.
    \item Построение результирующего массива по дереву сумм.
\end{enumerate}

Пусть длина исходного массива $2^d$. Тогда частичные суммы можно
вычислить по алгоритму \ref{alg:part-sum}.
\begin{algorithm}
\begin{algorithmic}
    \Procedure{PartSum}{$A[\;]$}
    \For{$k \gets 1$ to $d$}
        \For{$i \gets k$ to $d$}
            \State{$A[2^i]$ $\gets$ {$A[2^i] + A[2^{i - 1}]$}}
            \State{$i \gets i + k$}
        \EndFor
        \State{$k \gets k + 1$}
    \EndFor
    \EndProcedure
\end{algorithmic}
\caption{Построение частичных сумм}\label{alg:part-sum}
\end{algorithm}

Построение результирующего массива по массиву частичных
сумм производится по алгоритму \ref{alg:build-arr}.

\begin{algorithm}
\begin{algorithmic}
    \Procedure{BuildArray}{$A[\;]$}
    \State{$A[2^d] \gets 0$}
    \For{$k \gets d$ to $1$}
        \For{$i \gets d$ to $1$}
            \State{$s$ $\gets$ {$A[2^i] + A[2^{i - 1}]$}}
            \State{$A[2^{i - 1}]$ $\gets$ {$A[2^i]$}}
            \State{$A[2^i]$ $\gets$ $s$}
            \State{$i \gets i - k$}
        \EndFor
        \State{$k \gets k - 1$}
    \EndFor
    \EndProcedure
\end{algorithmic}
\caption{Построение префиксных сумм по массиву частичных сумм}\label{alg:build-arr}
\end{algorithm}

\section{Описание программы}

\begin{itemize}
    \item error.cuh -- cодержит макросы для обработки ошибок CUDA\_CHECK\_ERROR
        и \\
        CUDA\_CHECK\_LAST\_ERROR. Макросы выводят сообщение об ошибке
        и завершают программу.
    \item lab05.cu -- файл с ядрами hist, scan и make\_array.
\end{itemize}

После чтения массива из файла и копирования массива в память устройства,
строятся гистограмма и массив префиксных сумм,
выполняется построение результирующего массива.


\section{Результаты}

Сравнение производительности проводилось на массиве размера $10^8$.
В процессе профилирования были найдены оптимальные параметры для запуска
ядра hist на моей видеокарте.

\begin{table}
	\centering
	\begin{tabular}{|c|c|c|c|c|}
		\hline
		Ядро                 & Время работы \\ \hline
		hist<16, 256>        & 7.69ms       \\ \hline
		scan<1, 256>         & 4.06us       \\ \hline
		make\_result<1, 256> & 123.15ms     \\ \hline

	\end{tabular}
	\caption{Время работы ядер на GPU}
	\label{tab:results-gpu}
\end{table}

Для тестирования на CPU использовалась однопоточная версия данной
программы. Время выполнения составило $0.9$s.

\section{Выводы}
Для обеспечения высокой скорости работы программ на  GPU
часто бывает необходимо придумывать нестандартные алгоритмические решения.
Кроме того, медленные для CPU алгоритмы часто оказываются хорошо
распараллеливаемыми.
Однако, для достижения максимальной скорости работы на GPU
необходимо учитывать архитектурные особенности.
Например, при использовании разделяемой памяти следует избегать
конфликтов банков памяти.
Такая тонкая тонкая оптимизация позволяет получить максимальную
скорость выполнения программы на GPU.

\newpage
\section{Исходный код}
\lstinputlisting[style=clang]{lab05.cu}
\lstinputlisting[style=clang]{cpu_bench.cpp}

\end{document}
