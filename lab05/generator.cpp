#include <cstdint>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <stdexcept>
#include <vector>

int main(int argc, char** argv) {
    std::vector<int> hist(256, 0);

    if (argc < 3) {
        throw std::runtime_error("bad_arguments");
    }

    int size = std::stoi(argv[1]);
    std::string path(argv[2]);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 256);

    std::ofstream fs(path, std::ios::out | std::ios::binary);

    fs.write(reinterpret_cast<char*>(&size), sizeof(size));
    for (int i = 0; i < size; i++) {
        uint8_t rand = dis(gen);
        hist[rand]++;
        fs.write(reinterpret_cast<char*>(&rand), sizeof(rand));
    }

    for (int i = 0; i < 256; i++) {
        std::cout << i << ": " << hist[i] << std::endl;
    }
}
