#include <iostream>

void print_device_prop(cudaDeviceProp props) {
    std::cout << "Compute capability: " << props.major << "."
                                        << props.minor << std::endl;
    std::cout << "Name: " << props.name << std::endl;
    std::cout << "Global memory: " << props.totalGlobalMem << std::endl;
    std::cout << "Total shared per block: " << props.sharedMemPerBlock
              << std::endl;
    std::cout << "Total regs per block: " << props.regsPerBlock
              << std::endl;
    std::cout << "Max block size: " << props.maxThreadsDim[0] << ","
                                    << props.maxThreadsDim[1] << ","
                                    << props.maxThreadsDim[2]
              << std::endl;
    std::cout << "Max grid size: " << props.maxGridSize[0] << ","
                                   << props.maxGridSize[1] << ","
                                   << props.maxGridSize[2]
              << std::endl;
    std::cout << "Max threads per block: " << props.maxThreadsPerBlock
              << std::endl;
    std::cout << "Number of multiprocessors: " << props.multiProcessorCount
              << std::endl;
}

int main() {
    int dev_count;
    cudaGetDeviceCount(&dev_count);

    for (int i = 0; i < dev_count; i++) {
        cudaDeviceProp props;
        cudaGetDeviceProperties(&props, i);
        print_device_prop(props);
    }
}
