#include <helper/mapped_file.hpp>

#include <chrono>
#include <cmath>
#include <iostream>
#include <thread>
#include <vector>

namespace h = helper;

struct uchar4 {
    uint8_t x;
    uint8_t y;
    uint8_t z;
    uint8_t w;
};

void sobel(uchar4* img,
           uint64_t w,
           uint64_t h,
           uint64_t thread_id,
           uint64_t thread_count,
           uchar4* v) {
    
    auto y = [img, w, h](int i, int j) {
        if (i < 0) {
            i = 0;
        }
        else if (i > static_cast<int>(w - 1)) {
            i = w - 1;
        }

        if (j < 0) {
            j = 0;
        }
        else if (j > static_cast<int>(h - 1)) {
            j = h - 1;
        }

        auto p = img[j * w + i];
        return 0.299 * p.x / 255. + 0.587 * p.y / 255. + 0.114 * p.z / 255.;
    };

    uint64_t offset = w / thread_count + (w % thread_count == 0 ? 0 : 1);
    uint64_t offset_current = offset;
    if (w - offset * thread_id < offset) {
        offset_current  = w - offset * thread_id;
    }
    
    for (int i = thread_id * offset; i < thread_id * offset + offset_current; i++) {
        for (int j = 0; j < h; j++) {
            auto p = img[j * w + i];

            float gx = - y(i - 1, j - 1) - 2 * y(i, j - 1) - y(i + 1, j - 1)
                       + y(i - 1, j + 1) + 2 * y(i, j + 1) + y(i + 1, j + 1);
            float gy = y(i - 1, j - 1) + 2 * y(i - 1, j) + y(i - 1, j + 1)
                       - y(i + 1, j - 1) - 2 * y(i + 1, j) - y(i + 1, j + 1);
            
            uint8_t g = static_cast<uint8_t>(round(fminf(sqrt(gx * gx + gy * gy), 1.0) * 255));

            v[j * w + i] = uchar4{ g, g, g, p.w };
        }
    }
}

int main() {
    using of = h::mapped_file::open_flags;
  
    std::string file_name_in;
    std::string file_name_out;

    std::getline(std::cin, file_name_in);
    std::getline(std::cin, file_name_out);

    h::mapped_file fin;
    fin.open(file_name_in, of::ro, 2 * sizeof(uint32_t));

    uint32_t* size_ptr = reinterpret_cast<uint32_t*>(std::begin(fin));
    uint32_t w = *size_ptr++;
    uint32_t h = *size_ptr;

    uint64_t file_size = 2 * sizeof(uint32_t) + w * h * sizeof(uchar4);
    fin.remap(file_size);

    uchar4* data_in = reinterpret_cast<uchar4*>(std::begin(fin) + 2 * sizeof(uint32_t));
       
    h::mapped_file fout;
    fout.open(file_name_out, of::rw | of::c, file_size);

    size_ptr = reinterpret_cast<uint32_t*>(std::begin(fout));
    *size_ptr++ = w;
    *size_ptr = h;

    uchar4* data_out = reinterpret_cast<uchar4*>(std::begin(fout) + 2 * sizeof(uint32_t));
    
    std::vector<std::thread> threads;
    int thread_count = 5;

    for (int i = 1; i < thread_count; i++) {
        int ellapsed_seconds = 0;
        for (int j = 0; j < 5; j++) {
            std::chrono::time_point<std::chrono::system_clock> start, end;
            start = std::chrono::system_clock::now();
    
            for (int thread_id = 0; thread_id < i; thread_id++) {
                 threads.push_back(std::thread(sobel, data_in, w, h, thread_id, i, data_out));
            }

            for (auto& thread : threads) {
                thread.join();
            }

            end = std::chrono::system_clock::now();
            ellapsed_seconds += std::chrono::duration_cast<std::chrono::seconds>
                                 (end-start).count();
    
            threads.clear();
        }
        std::cerr << i << ": " << ellapsed_seconds / 5.0 << std::endl;
    }

    fin.close();
    fout.close();
}
