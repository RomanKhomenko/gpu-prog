#include <helper/error.cuh>
#include <helper/mapped_file.hpp>
#include <helper/vector.cuh>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <limits>

namespace h = helper;

template<class T>
static cudaArray* make_cuda_array(uint64_t w, uint64_t h, const T* data) {
    cudaError_t err;
    cudaArray* dev_arr;
    auto cdesc = cudaCreateChannelDesc<T>();
    
    err = cudaMallocArray(&dev_arr, &cdesc, w, h);
    CUDA_CHECK_ERROR(err);
    
    if (data != nullptr) {
        err = cudaMemcpyToArray(dev_arr, 0, 0, data, sizeof(T) * w * h,
                                cudaMemcpyHostToDevice);
        CUDA_CHECK_ERROR(err);
    }

    return dev_arr;
}

__global__ void sobel(cudaTextureObject_t tex,
                      uint64_t w,
                      uint64_t h,
                      h::vector<uchar4> v) {
    auto y = [&tex](int i, int j) {
        auto p = tex2D<uchar4>(tex, i, j);
        return 0.299 * p.x / 255. + 0.587 * p.y / 255. + 0.114 * p.z / 255.;
    };

    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    int idy = threadIdx.y + blockDim.y * blockIdx.y;
    int offsetx = blockDim.x * blockDim.x;
    int offsety = blockDim.y * blockDim.y;

    for (int i = idx; i < w; i += offsetx) {
        for (int j = idy; j < h; j += offsety) {
            auto p = tex2D<uchar4>(tex, i, j);
            float gx = - y(i - 1, j - 1) - 2 * y(i, j - 1) - y(i + 1, j - 1)
                       + y(i - 1, j + 1) + 2 * y(i, j + 1) + y(i + 1, j + 1);
            float gy = y(i - 1, j - 1) + 2 * y(i - 1, j) + y(i - 1, j + 1)
                       - y(i + 1, j - 1) - 2 * y(i + 1, j) - y(i + 1, j + 1);
            
            uint8_t g = static_cast<uint8_t>(round(fminf(sqrt(gx * gx + gy * gy), 1.0) * 255));

            v[j * w + i] = make_uchar4(g, g, g, p.w);
        }
    }
}

int main() {
    using of = h::mapped_file::open_flags;
  
    cudaError_t err;
  
    std::string file_name_in;
    std::string file_name_out;

    std::getline(std::cin, file_name_in);
    std::getline(std::cin, file_name_out);

    h::mapped_file fin;
    fin.open(file_name_in, of::ro, 2 * sizeof(uint32_t));

    uint32_t* size_ptr = reinterpret_cast<uint32_t*>(std::begin(fin));
    uint32_t w = *size_ptr++;
    uint32_t h = *size_ptr;

    uint64_t file_size = 2 * sizeof(uint32_t) + w * h * sizeof(uchar4);
    fin.remap(file_size);

    uchar4* data_in = reinterpret_cast<uchar4*>(std::begin(fin) + 2 * sizeof(uint32_t));
    
    auto dev_arr_in = make_cuda_array(w, h, data_in);
    auto dev_arr_out = h::vector<uchar4>(w * h);
 
    cudaResourceDesc rdesc = {};
    rdesc.resType = cudaResourceTypeArray;
    rdesc.res.array.array = dev_arr_in;

    cudaTextureDesc tdesc = {};
    tdesc.addressMode[0] = cudaAddressModeClamp;
    tdesc.addressMode[1] = cudaAddressModeClamp;
    tdesc.filterMode = cudaFilterModePoint;
    tdesc.normalizedCoords = false;
    tdesc.readMode = cudaReadModeElementType;

    cudaTextureObject_t tex = {};
    err = cudaCreateTextureObject(&tex, &rdesc, &tdesc, nullptr);
    CUDA_CHECK_ERROR(err);
        
    sobel<<< dim3(32, 32), dim3(32, 32) >>>(tex, w, h, dev_arr_out);
    cudaDeviceSynchronize();

    err = cudaDestroyTextureObject(tex);
    CUDA_CHECK_ERROR(err);
   
    h::mapped_file fout;
    fout.open(file_name_out, of::rw | of::c, file_size);

    size_ptr = reinterpret_cast<uint32_t*>(std::begin(fout));
    *size_ptr++ = w;
    *size_ptr = h;

    uchar4* data_out = reinterpret_cast<uchar4*>(std::begin(fout) + 2 * sizeof(uint32_t));
    dev_arr_out.to_host(data_out);
    
    cudaFreeArray(dev_arr_in);

    fin.close();
    fout.close();
}
